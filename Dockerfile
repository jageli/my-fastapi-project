# 使用官方Python基础镜像
FROM python:3.9.14

# 设置代理服务器
ENV http_proxy "http://10.144.1.10:8080"
ENV https_proxy "http://10.144.1.10:8080"

# 设置工作目录
WORKDIR /app

# 复制requirements.txt到工作目录
COPY app/requirements.txt .

# 安装Python依赖
RUN pip install --no-cache-dir -r requirements.txt

# 复制应用代码到工作目录
COPY app .

# 暴露FastAPI应用的端口（默认8000）
EXPOSE 8000

# 启动FastAPI应用
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
