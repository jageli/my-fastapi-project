import requests

class TestLineList:
    def __init__(self, username, password):
        self.session = requests.Session()
        self.login_info = {'username': username, 'password': password}
        self.login_url = 'http://compass.dyn.nesc.nokia.net:8080/common/login_handle/'
        self.device_info_url = 'http://compass.dyn.nesc.nokia.net:8080/device_info/config/export/'
        self.device_info_params = 'sg=%5B%5D&tl=%5B%5D&owner=%5B%5D&task=%5B%5D'

    def login(self):
        response = self.session.post(self.login_url, data=self.login_info)
        return response.status_code == 200

    def download_file(self,file_path):
        if self.login():
            print("登录compass成功")
            response = self.session.post(self.device_info_url, data=self.device_info_params, headers={'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})
            if response.status_code == 200:
                print("数据获取成功")
                with open(file_path, 'wb') as file:
                    file.write(response.content)
                    print(f"文件已成功下载并保存到 {file_path}")
            else:
                print("获取设备信息失败")
                return None
        else:
            print("登录失败")
            return None