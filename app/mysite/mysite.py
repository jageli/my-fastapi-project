import logging
from fastapi import APIRouter, File, UploadFile
from starlette.templating import Jinja2Templates
from starlette.requests import Request
import httpx
import xlrd

from mysite.Test_Line_List import TestLineList
from config import init_config
from main_api.get_key import UserDecryptor
from mysite.HW_comparation_audit import HWComparisonAudit

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

router = APIRouter()
templates = Jinja2Templates(directory="./mysite/templates")


@router.get("/", summary="mysite 主页 ")
async def main(request: Request):
    testline_list = get_testline_inform()
    return templates.TemplateResponse(
        "mysite.html", {"request": request, "testline_list": testline_list}
    )


@router.get("/HWComparison", summary="HWComparison")
async def HWComparison():
    decryptor = UserDecryptor(key=init_config.KEY_STR)
    user_key = decryptor.run()
    audit = HWComparisonAudit("j7li", "Iopjkl_123")
    data = audit.fetch_device_info()
    if data:
        processed_data = audit.process_data(data)
    audit.save_to_html(
        processed_data, path="./mysite/static/html/HWs_not_install_audit.html"
    )


@router.get("/TestLineConfig", summary="TestLineConfig")
async def TestLineConfig():
    decryptor = UserDecryptor(key=init_config.KEY_STR)
    user_key = decryptor.run()
    # myTestLineList = TestLineList(user_key["username"], user_key["password"])
    myTestLineList = TestLineList("j7li", "Iopjkl_123")
    myTestLineList.download_file("./mysite/resource/TestEnvironment.xlsx")
    return "Download success!"


@router.post("/mysite_upload_excle", summary=" ", description="上传硬件表格")
async def Upload_excle(file: UploadFile = File(...)):
    save_file = f"./mysite/resource/TestEnvironment.xlsx"
    content = await file.read()
    with open(save_file, "wb") as f:
        f.write(content)
    return "Upload success!"


def get_testline_inform():
    path = "./mysite/resource/TestEnvironment.xlsx"
    data = xlrd.open_workbook(path)
    table_id = data.sheet_by_name("TL List")
    test_line_id = table_id.col_values(0)[1:]
    gnb_ip = table_id.col_values(1)[1:]
    enb_ip = table_id.col_values(8)[1:]
    Owner = table_id.col_values(4)[1:]

    my_list = []

    for i in range(0, len(test_line_id)):
        if "_vDU" not in test_line_id[i] and "_vCU" not in test_line_id[i]:
            TLNAME = "{}".format(test_line_id[i].replace("7_5_", ""))
            gnb = "{}".format(gnb_ip[i])

            if enb_ip[i] != "":
                enb = "{}".format(enb_ip[i])
            else:
                enb = ""
            my_list.append([TLNAME, gnb, enb, Owner[i]])
    return my_list
