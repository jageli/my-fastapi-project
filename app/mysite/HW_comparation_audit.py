import requests
import pandas as pd

class HWComparisonAudit:
    def __init__(self, username, password):
        self.session = requests.Session()
        self.login_info = {'username': username, 'password': password}
        self.login_url = 'http://compass-5g.eecloud.dynamic.nsn-net.net:8000/common/login_handle/'
        self.device_info_url = 'http://compass-5g.eecloud.dynamic.nsn-net.net:8000/device_info/hw_comparison/'
        self.device_info_params = 'sg=%5B%5D&status=%5B%5D&hw=%5B%5D&category=%5B%5D&user=%5B%5D'

    def login(self):
        response = self.session.post(self.login_url, data=self.login_info)
        return response.status_code == 200

    def fetch_device_info(self):
        if self.login():
            print("登录成功")
            response = self.session.post(self.device_info_url, data=self.device_info_params, headers={'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})
            if response.status_code == 200:
                print("数据获取成功")
                return response.json()['data']
            else:
                print("获取设备信息失败")
                return None
        else:
            print("登录失败")
            return None

    def process_data(self, data):
        df = pd.DataFrame(data)
        columns_to_keep = ["device_name", "user_email", "user_sg", "serial_number", "location", "extra_comment", "hw_audit", "not_installed_days", "last_installed_tl"]
        df = df[columns_to_keep]
        # 应用筛选条件
        # user_sg 筛选
        sg_filters = ['MN RAN RD VRF HAZ4 4']
        df = df[df['user_sg'].apply(lambda x: any(sg in (x if x is not None else '') for sg in sg_filters))]

        # hw_audit 筛选
        df = df[df['hw_audit'].isin(['Yes, Active Device'])]

        # not_installed_days 大于 2 筛选
        df = df[df['not_installed_days'].astype(int) > 2]

        # extra_comment 不等于 SFP on Hostnic 筛选
        df = df[df['extra_comment'] != 'SFP on Hostnic']
        return df

    def colorize_row(self, row):
        bg_color = ''
        if row['not_installed_days'] > 13:
            bg_color = 'background-color: red'
        elif row['not_installed_days'] > 6:
            bg_color = 'background-color: yellow'
        return [bg_color] * len(row)

    def save_to_html(self, df,path="./mysite/templates/HWs_not_install_audit.html"):
        styled_df = df.style.apply(self.colorize_row, axis=1).set_table_styles([{'selector': 'th, td', 'props': [('text-align', 'center')]}])
        styled_df.to_html(path, index=False, encoding='utf-8')
        print("筛选后的数据已转换并保存到HWs_not_install_audit.html")

# 使用示例
# audit = HWComparisonAudit("j7li", "Taotao_123")
# data = audit.fetch_device_info()
# if data:
#     processed_data = audit.process_data(data)
#     audit.save_to_html(processed_data)