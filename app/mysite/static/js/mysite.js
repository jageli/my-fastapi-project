window.onload = function () {
    //需求：当用户点击某个模块的时候，给该模块添加样式，对应的内容显示出来
    //获取所有的a标签
    var navs = document.querySelectorAll("nav a");
    //先显示一部分内容（H5标签选择器默认选每一类标签的第一个）
    document.querySelector("section").style.display = "block";
    //遍历该数组
    for (var i = 0; i < navs.length; i++) {
        navs[i].onclick = function () {
            //每次点击前都先清除原来的内容
            var beforeNav = document.querySelector(".active")
            var beforeId = beforeNav.dataset["cont"];
            document.querySelector("#" + beforeId).style.display = "none";
            //排他思想
            for (var j = 0; j < navs.length; j++) {
                //先去除所有的active标签
                navs[j].classList.remove("active");
            }
            //对应的a加样式
            this.classList.add("active");
            //获取对应的内容标签并添加样式
            var secId = this.dataset["cont"];
            document.querySelector("#" + secId).style.display = "block";
        }
    }

}


document.addEventListener("DOMContentLoaded", function () {
    // 处理按钮点击事件
    document.getElementById('TestLineList').addEventListener('click', UpdateTestlineList);
    document.getElementById('UpdateHWComparison').addEventListener('click', UpdateHWComparisonButtonClick);
});

function UpdateTestlineList() {
    this.disabled = true;
    console.log('Button clicked');
    fetch('http://10.101.43.144:8000/mysite/TestLineConfig')
        .then(response => {
            console.log('Received response', response);
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            // 接口调用成功后，刷新页面
            location.reload();

        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        })
        .finally(() => {
            // 不管请求成功还是失败，最后都会执行，确保按钮重新可用
            this.disabled = false;
        });
}

function UpdateHWComparisonButtonClick() {
    this.disabled = true;
    console.log('Button clicked');

    var section = document.getElementById('HW');
    var iframes = document.querySelectorAll('#HW iframe');
    iframes.forEach(function (iframe) {
        if (iframe.parentNode === section) {
            section.removeChild(iframe);
        }
    });
    fetch('http://10.70.224.18:8000/mysite/HWComparison')
        .then(response => {
            console.log('Received response', response);
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            // 接口调用成功后，创建并插入iframe
            var iframe = document.createElement('iframe');
            iframe.src = "mysite_static/html/HWs_not_install_audit.html";
            iframe.width = "100%";
            iframe.height = "600px";
            iframe.style.border = "none";

            // 将iframe添加到页面的指定位置，例如body的末尾
            section.appendChild(iframe);

        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        })
        .finally(() => {
            // 不管请求成功还是失败，最后都会执行，确保按钮重新可用
            this.disabled = false;
        });
}


