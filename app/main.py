
from app import FastAPIAdmin


app_main = FastAPIAdmin(
    title="FastAPI Admin",
    description="FastAPI Admin is a admin panel for FastAPI.",
    version="0.0.1",
    # auth_url='/auth'
)
app = app_main.app
