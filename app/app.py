from fastapi import FastAPI, applications
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html
from mysite.mysite import router as mysite_api
from main_api.main_api import router as main_api
from onepiece_note.onepiece import router as onepiece_note_api

class FastAPIAdmin:
    def __init__(
        self,
        title: str,
        description: str,
        version: str,
        docs_url: str = "/docs",
        redoc_url: str = "/redocs",
    ):
        self.app = FastAPI(
            title=title,
            description=description,
            version=version,
            docs_url=docs_url,
            redoc_url=redoc_url,
        )
        self.app.mount(
            path="/static", app=StaticFiles(directory="./static"), name="static"
        )
        self.configure_swagger_ui()
        self.main_api()
        self.my_site_api()
        self.onepiece_api()

    def configure_swagger_ui(self):
        def monkey_patch_swagger_ui(*args, **kwargs):
            """
            Wrap the function which is generating the HTML for the /docs endpoint and
            overwrite the default values for the swagger js and css.
            """
            return get_swagger_ui_html(
                *args,
                **kwargs,
                swagger_js_url="static/js/swagger-ui-bundle.js?v=5.9.0",
                swagger_css_url="static/css/swagger-ui.css?v=5.9.0",
                swagger_favicon_url="static/images/favicon.png?v=5.9.0",
            )

        applications.get_swagger_ui_html = monkey_patch_swagger_ui

    def main_api(self):
        self.app.include_router(main_api)

    def include_router(self, router):
        self.app.include_router(router)

    def add_root_route(self, route: str, endpoint: str):
        self.app.get(route)(lambda: {"Hello": endpoint})

    def my_site_api(self):
        mysite = FastAPI(
            title="mysite",
            description="mysite api page",
            version="v1",
            docs_url="/docs",
        )
        mysite.mount(
            path="/static",
            app=StaticFiles(directory="./static"),
            name="static",
        )
        mysite.mount(
            path="/mysite_static",
            app=StaticFiles(directory="./mysite/static"),
            name="mysite_static",
        )
        mysite.include_router(mysite_api)
        self.app.mount("/mysite", mysite)
    
    def onepiece_api(self):
        mysite = FastAPI(
            title="onepiece",
            description="onepiece api page",
            version="v1",
            docs_url="/docs",
        )
        mysite.mount(
            path="/static",
            app=StaticFiles(directory="./static"),
            name="static",
        )
        mysite.mount(
            path="/onepiece_static",
            app=StaticFiles(directory="./onepiece_note/static"),
            name="onepiece_static",
        )
        mysite.include_router(onepiece_note_api)
        self.app.mount("/onepiece_note", mysite)