from fastapi import FastAPI, applications
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html
from api1.api1 import router as api1_router

app = FastAPI(
    title="FastAPI Admin",
    description="",
    version="v1",
    docs_url="/docs",
    redoc_url="/redocs",
)

app.mount(path="/static", app=StaticFiles(directory="./static"), name="static")


def monkey_patch_swagger_ui(*args, **kwargs):
    """
    Wrap the function which is generating the HTML for the /docs endpoint and
    overwrite the default values for the swagger js and css.
    """
    return get_swagger_ui_html(
        *args,
        **kwargs,
        swagger_js_url="static/js/swagger-ui-bundle.js?v=5.9.0",
        swagger_css_url="static/css/swagger-ui.css?v=5.9.0",
        swagger_favicon_url="static/images/favicon.png?v=5.9.0",
    )


applications.get_swagger_ui_html = monkey_patch_swagger_ui


@app.get("/")
def root():
    return {"Hello": "World"}


@app.get("/api/v1/test")
def test_root():
    return {"Hello": "World1"}


from subapi1.subapi1 import router as subapi1_router

app.include_router(api1_router, tags=["api1"])

subapi = FastAPI(
    title="subapi",
    description="子程序",
    version="v1",
    docs_url="/docs",
)
subapi.mount(
    path="/static", app=StaticFiles(directory="./static"), name="static"
)
subapi.include_router(subapi1_router, tags=["subapi1"])

app.mount("/subapi", subapi)
