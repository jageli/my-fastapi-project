import logging
from fastapi import APIRouter
from starlette.templating import Jinja2Templates
from starlette.requests import Request


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

router = APIRouter()
templates = Jinja2Templates(directory="./onepiece_note/templates")


@router.get("/", summary="onepiece admin 主页 ")
async def main(request: Request):
    return templates.TemplateResponse(
        "admin.html", {"request": request}
    )
