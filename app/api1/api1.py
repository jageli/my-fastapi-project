from fastapi import APIRouter

router = APIRouter(tags=["api1"])


@router.get("/api/v1/vdu", summary="get vDU state data")
def test():
    return {"data": "vDU state data"}
