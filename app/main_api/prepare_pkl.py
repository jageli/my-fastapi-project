from pathlib import Path
import pickle
from cryptography.fernet import Fernet


class UserEncryptor:
    def __init__(self):
        self.key = Fernet.generate_key()
        self.cipher_suite = Fernet(self.key)

    def encrypt_user(self, data):
        if "password" in data:
            # 使用 Fernet 加密密码
            encoded_password = data["password"].encode("utf-8")
            encrypted_password = self.cipher_suite.encrypt(encoded_password)
            data["password"] = encrypted_password.decode("utf-8")
        return data

    # def save_user(self, data):
    #     user = self.encrypt_user(data=data)
    #     with open("user.pkl", "wb") as f:
    #         pickle.dump(user, f)

    def save_key(self):
        # 创建一个key文件夹
        file_path = Path("store","key", "key.pkl")
        file_path.parent.mkdir(parents=True, exist_ok=True)

        # key是一个bytes类型的密钥,转换成字符串类型并保存
        with open(file_path, "wb") as f:
            pickle.dump(self.key, f)
        return file_path

    def prepare_key_file(self, data):
        # data = {"username": username, "password": password}
        # self.save_user(data)
        file_path = self.save_key()
        key = self.key.decode("utf-8")
        return file_path, key


# 使用示例
# encryptor = UserEncryptor()
# encryptor.main("user1", "password123")
