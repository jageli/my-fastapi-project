import pickle
from cryptography.fernet import Fernet

class UserDecryptor:
    def __init__(self, key="RKN6pXBVFMMp3B-j2YBJMN0UvRsNhNrY6oTWLJk827Q="):
        self.key = bytes(key, encoding="utf-8")
        self.cipher_suite = Fernet(self.key)

    def decrypt_user(self, data):
        if "password" in data:
            encrypted_password = data["password"].encode("utf-8")
            decrypted_password = self.cipher_suite.decrypt(encrypted_password)
            data["password"] = decrypted_password.decode("utf-8")
        return data

    def load_user(self, filename="store/key/key.pkl"):
        with open(filename, "rb") as f:
            user = pickle.load(f)
            user_key = self.decrypt_user(user)
        return user_key

    def run(self):
        user_key= self.load_user()
        return user_key
        