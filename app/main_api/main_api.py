import os
from typing import Dict, List
from fastapi import APIRouter, Body, HTTPException, Request
from fastapi.responses import FileResponse, JSONResponse
from starlette.status import HTTP_201_CREATED
from main_api.get_key import UserDecryptor
from main_api.prepare_pkl import UserEncryptor

router = APIRouter(tags=["main"])


@router.get("/", summary="main api")
def main():
    return {"data": "hello main"}


Example = {"username": "username", "password": "password"}
shared_folder_path = "store"


@router.get("/store", response_model=List[Dict[str, str]])
async def list_files():
    """列出共享文件夹中的所有文件和文件夹"""
    items = os.listdir(shared_folder_path)
    result = []
    for item in items:
        item_path = os.path.join(shared_folder_path, item)
        if os.path.isdir(item_path):
            result.append({"name": item, "type": "folder"})
        else:
            result.append({"name": item, "type": "file"})
    return result


@router.get("/store/{file_path:path}")
async def download_file(file_path: str):
    # 假设你的文件存储在服务器的某个目录下，这里需要根据实际情况调整路径
    file_location = f"{file_path}"
    try:
        return FileResponse(path=file_location, filename=file_path)
    except Exception as e:
        raise HTTPException(status_code=404, detail=f"File not found: {e}")


@router.post("/pkl", summary="prepare pkl file")
def prepare_pkl_file_and_key(request: Request, data: dict = Body(..., example=Example)):
    encryptor = UserEncryptor()
    try:
        file_path, key = encryptor.prepare_key_file(data)
        # 假设你有一个函数来生成文件的下载URL
        file_url = generate_file_download_url(request, file_path)
        return JSONResponse(
            status_code=HTTP_201_CREATED,
            content={"file_url": file_url, "key": key},
        )
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))


def generate_file_download_url(request: Request, file_path: str):
    # 使用Request对象来构建完整的URL，包括协议、主机名和端口
    # 这样可以确保URL适用于任何部署环境
    base_url = str(request.base_url)
    return f"{base_url}store/{file_path}"


@router.post("/get_key", summary="get key")
def get_key(key: str = "5_OnK5L5MQw8b0YTS5K_tkPLk_rtB_8F1tJzt62JWTM="):
    decryptor = UserDecryptor(key=key)
    user_key = decryptor.run()
    # return user_key
